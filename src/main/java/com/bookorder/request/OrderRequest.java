package com.bookorder.request;

import java.util.List;

public class OrderRequest {
	private List<Integer> order;

	public List<Integer> getOrder() {
		return order;
	}

	public void setOrder(List<Integer> order) {
		this.order = order;
	}
	
	
}
