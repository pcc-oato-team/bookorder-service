package com.bookorder.response;

import java.util.Date;

public class FailureResponse<T> {
	
	private int status;
	private String message;

	public FailureResponse() {
	}

	public FailureResponse( int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getTimestamp() {
		return new Date();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
