package com.bookorder.response;

import java.util.List;

public class SumPriceResponse {
	private Double sumPrice;
	private List<OrderResponse> orders ;

	public Double getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice(Double sumPrice) {
		this.sumPrice = sumPrice;
	}

	public List<OrderResponse> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderResponse> orders) {
		this.orders = orders;
	}

}
