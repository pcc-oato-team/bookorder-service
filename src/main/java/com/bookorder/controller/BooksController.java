package com.bookorder.controller;

import java.util.Map;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.bookorder.exception.GlobalResponseEntityExceptionHandler;
import com.bookorder.exception.NotFoundException;
import com.bookorder.request.CreateBookRequest;
import com.bookorder.request.OrderRequest;
import com.bookorder.response.BookResponse;
import com.bookorder.response.SumPriceResponse;
import com.bookorder.service.BookService;

@RestController
@RequestMapping("/books")
public class BooksController {
	private static final Logger logger = LoggerFactory.getLogger(BooksController.class);
	@Autowired
	private BookService bookService;

	@GetMapping()
	public ResponseEntity<Map<String, Object>> findListBook(
			@RequestParam(required = false, value = "keyword") String keyword,
			@RequestParam(required = false, value = "filter") String filter,
			@RequestParam(value = "pageIndex") Integer pageIndex, @RequestParam(value = "pageSize") Integer pageSize) {

		Map<String, Object> book = bookService.getListBook(keyword, filter, pageSize, pageIndex);
		return ResponseEntity.ok(book);
	}

	@SuppressWarnings("unchecked")
	@PostMapping()
	public ResponseEntity<BookResponse> createbook(@Valid @RequestBody CreateBookRequest createBookRequest) {
		GlobalResponseEntityExceptionHandler globalResponse = new GlobalResponseEntityExceptionHandler();

		if (createBookRequest.getName().isEmpty()) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Name Not Found"));
		}
		if (createBookRequest.getAuthor().isEmpty()) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Author Not Found"));
		}
		if (createBookRequest.getPrice() == null) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Price Not Found"));
		}
		if (createBookRequest.getIs_recommended() == null) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Is_recommended Not Found"));
		}
		BookResponse order = bookService.createOrder(createBookRequest);
		order.setId(createBookRequest.getId());
		order.setName(createBookRequest.getName());
		order.setAuthor(createBookRequest.getAuthor());
		order.setPrice(createBookRequest.getPrice());
		order.setIs_recommended(createBookRequest.getIs_recommended());
		return ResponseEntity.status(HttpStatus.OK).body(order);
	}

	@SuppressWarnings("unchecked")
	@DeleteMapping("/{id}")
	public ResponseEntity<BookResponse> deleteBook(@PathVariable Integer id) {
		GlobalResponseEntityExceptionHandler globalResponse = new GlobalResponseEntityExceptionHandler();
		if (!bookService.deleteBook(id)) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Id Not Found"));
		}
		return ResponseEntity.ok().build();
	}

	@SuppressWarnings("unchecked")
	@PutMapping
	public ResponseEntity<BookResponse> updateBook(@RequestBody CreateBookRequest request) {
		GlobalResponseEntityExceptionHandler globalResponse = new GlobalResponseEntityExceptionHandler();
		if (request.getId() == null) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Id Not Found"));
		}
		if (request.getName().isEmpty()) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Name Not Found"));
		}
		if (request.getAuthor().isEmpty()) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Author Not Found"));
		}
		if (request.getPrice() == null) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Price Not Found"));
		}
		if (request.getIs_recommended() == null) {
			return globalResponse.handleBookNotFoundException(new NotFoundException("Is_recommended Not Found"));
		}

		BookResponse bookResponse = bookService.updateBookService(request);
		return ResponseEntity.ok(bookResponse);

	}

	@SuppressWarnings("unchecked")
	@PostMapping(value = "/orders")
	public ResponseEntity<SumPriceResponse> calOrder(@RequestBody OrderRequest request) {
		GlobalResponseEntityExceptionHandler globalResponse = new GlobalResponseEntityExceptionHandler();
		try {

			SumPriceResponse sumPriceRequest = bookService.calOrderService(request);
			if (sumPriceRequest.getOrders().isEmpty()) {
				return globalResponse.handleBookNotFoundException(new NotFoundException("Order Not Found"));
			}
			return ResponseEntity.status(HttpStatus.OK).body(sumPriceRequest);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<BookResponse> getBookListId(@PathVariable Integer id) {
		BookResponse book = bookService.getBook(id);
		return ResponseEntity.ok(book);
	}

}
