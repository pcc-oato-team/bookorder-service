package com.bookorder.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bookorder.exception.NotFoundException;
import com.bookorder.model.Book;
import com.bookorder.repository.BookRepository;
import com.bookorder.request.CreateBookRequest;
import com.bookorder.request.OrderRequest;
import com.bookorder.response.BookResponse;
import com.bookorder.response.OrderResponse;
import com.bookorder.response.SumPriceResponse;

@Service
public class BookService {
	@Autowired
	private BookRepository bookRepository;

	@Autowired
	public BookService(BookRepository repository) {
		this.bookRepository = repository;
	}

	public BookResponse createOrder(CreateBookRequest request) {
		Optional<Book> bookOptional = bookRepository.findByName(request.getName());
		if (bookOptional.isPresent()) {
			throw new NotFoundException("Duplicate Book Name");
		}
		Book book = new Book();
		BookResponse bookResponse = new BookResponse();
		book.setName(request.getName());
		book.setAuthor(request.getAuthor());
		book.setPrice(request.getPrice());
		book.setIs_recommended(request.getIs_recommended());
		bookRepository.save(book);
		bookResponse.setName(book.getName());
		bookResponse.setAuthor(book.getAuthor());
		bookResponse.setIs_recommended(book.getIs_recommended());
		bookResponse.setPrice(book.getPrice());

		return bookResponse;
	}

	public List<BookResponse> getAllBook() {
		List<Book> bookList = bookRepository.getBookRepository();
		List<BookResponse> bookResponse = new ArrayList<BookResponse>();
		for (Book b : bookList) {
			BookResponse book = new BookResponse();
			book.setId(b.getId());
			book.setName(b.getName());
			book.setAuthor(b.getAuthor());
			book.setPrice(b.getPrice());
			book.setIs_recommended(b.getIs_recommended());
			bookResponse.add(book);
		}

		return bookResponse;
	}

	public Map<String, Object> getListBook(String keyword, String filter, Integer pageSize, Integer pageIndex) {
		Integer offSet = 0;
		Integer limit = 0;
		if (StringUtils.isEmpty(keyword)) {
			keyword = "";
		}
		if (StringUtils.isEmpty(filter)) {
			filter = "";
		}

		offSet = (pageSize * pageIndex) + 1;
		limit = pageSize;

		List<Book> bookList = bookRepository.getFindBook(keyword, filter, limit, offSet);
		Optional<Book> bookOptional = bookRepository.countTotal();
		List<BookResponse> bookResponse = new ArrayList<BookResponse>();

		Map<String, Object> bookMap = new HashMap<>();
		bookMap.put("data", bookResponse);
		bookMap.put("total", bookOptional);

		for (Book b : bookList) {
			BookResponse book = new BookResponse();
			book.setId(b.getId());
			book.setName(b.getName());
			book.setAuthor(b.getAuthor());
			book.setPrice(b.getPrice());
			book.setIs_recommended(b.getIs_recommended());
			bookResponse.add(book);

		}

		return bookMap;
	}

	public boolean deleteBook(int id) {
		try {
			bookRepository.deleteById(id);
			return true;
		} catch (EmptyResultDataAccessException e) {
			return false;
		}
	}

	public BookResponse updateBookService(CreateBookRequest request) {
		Optional<Book> bookOptional = bookRepository.findById(request.getId());
		Optional<Book> bookOptionals = bookRepository.findByName(request.getName());
		BookResponse bookResponse = new BookResponse();

		if (bookOptionals.isPresent()) {
			throw new NotFoundException("Duplicate Book Name");
		}
		if (bookOptional.isPresent()) {

		} else {
			throw new NotFoundException("BookId Not Found");
		}
		Book book = bookOptional.get();
		book.setId(request.getId());
		book.setName(request.getName());
		book.setAuthor(request.getAuthor());
		book.setPrice(request.getPrice());
		book.setIs_recommended(request.getIs_recommended());
		bookRepository.save(book);
		bookResponse.setId(book.getId());
		bookResponse.setName(book.getName());
		bookResponse.setAuthor(book.getAuthor());
		bookResponse.setPrice(book.getPrice());
		bookResponse.setIs_recommended(book.getIs_recommended());

		return bookResponse;
	}

	public SumPriceResponse calOrderService(OrderRequest request) {
		List<Book> book = bookRepository.findAllById(request.getOrder());
		List<OrderResponse> orderResponse = new ArrayList<>();
		Double sumPrice = 0d;
		SumPriceResponse sum = new SumPriceResponse();

		for (Book b : book) {
			OrderResponse or = new OrderResponse();
			sumPrice += b.getPrice();
			or.setId(b.getId());
			or.setName(b.getName());
			or.setAuthor(b.getAuthor());
			or.setIs_recommended(b.getIs_recommended());
			or.setPrice(b.getPrice());
			orderResponse.add(or);
		}
		sum.setOrders(orderResponse);
		sum.setSumPrice(sumPrice);
		return sum;
	}

	public BookResponse getBook(Integer id) {
		Optional<Book> books = bookRepository.findById(id);
		BookResponse book = new BookResponse();
		if (books.isPresent()) {
			Book b = books.get();
			book.setId(b.getId());
			book.setName(b.getName());
			book.setAuthor(b.getAuthor());
			book.setPrice(b.getPrice());
			book.setIs_recommended(b.getIs_recommended());
		} else {
			throw new NotFoundException("BookId Not Found");
		}

		return book;
	}

	/*
	 * public List<BookResponse> findByName(String name) { List<Book> bookList =
	 * bookRepository.getFindBook(name); List<BookResponse> bookResponse = new
	 * ArrayList<BookResponse>(); for (Book b : bookList) { BookResponse book = new
	 * BookResponse(); book.setId(b.getId()); book.setName(b.getName());
	 * book.setAuthor(b.getAuthor()); book.setPrice(b.getPrice());
	 * book.setIs_recommended(b.getIs_recommended()); bookResponse.add(book); }
	 * return bookResponse; }
	 */
}
