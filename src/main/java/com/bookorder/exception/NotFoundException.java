package com.bookorder.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class NotFoundException extends RuntimeException{
	private static final long serialVersionUID = -5902194089070566061L;

	public NotFoundException(String exception) {
	    super(exception);
	  }
}
