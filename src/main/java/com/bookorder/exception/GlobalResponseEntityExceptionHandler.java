package com.bookorder.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bookorder.response.FailureResponse;

@ControllerAdvice
public class GlobalResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity handleBookNotFoundException(NotFoundException ex) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		FailureResponse<String> response = new FailureResponse<>();
		response.setStatus(status.value());
		response.getTimestamp();
		response.setMessage(ex.getMessage());

		return new ResponseEntity(response, status);
	}

}
