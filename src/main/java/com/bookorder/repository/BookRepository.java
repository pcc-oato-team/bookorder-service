package com.bookorder.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.bookorder.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

	@Query("SELECT c FROM Book c ORDER BY c.is_recommended DESC")
	List<Book> getBookRepository();

	@Query(value = "SELECT c.* FROM Book c WHERE (('r' = :filter AND c.is_recommended = true) OR 'a' = :filter OR '' = :filter)" + 
			"	AND (c.id LIKE '%' || :keyword || '%'" + 
			"		OR c.name LIKE '%' || :keyword || '%'" + 
			"		OR c.author LIKE '%' || :keyword || '%'" + 
			"	)" + 
			"ORDER BY c.is_recommended DESC LIMIT :limit OFFSET :offSet" ,nativeQuery = true)
	List<Book> getFindBook(@Param("keyword") String keyword,@Param("filter") String filter,@Param("limit") Integer limit,@Param("offSet") Integer offSet);

	
	@Query("SELECT c FROM Book c WHERE c.is_recommended = true")
	List<Book> getis_recommendedRepository(@Param("is_recommended") boolean is_recommended);
	
	
	Optional<Book> findByName(String name);
	
	@Query("SELECT  COUNT(d.id) FROM Book d")
	Optional<Book> countTotal();
	
	/*
	 * SELECT c.* FROM bookstoredb.book c WHERE (('r' = 'a' AND c.is_recommended
	 * =true) OR 'a' = 'a') AND (c.id LIKE '%%' OR c.name LIKE '%%' OR c.author
	 * LIKE'%%' ) ORDER BY c.is_recommended DESC limit 7;
	 */
}
